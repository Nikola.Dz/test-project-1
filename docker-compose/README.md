# Build

docker-compose up -d --build

// had some problems, --build took care of them

## Test

$ echo $(docker network inspect docker_default | grep Gateway | grep -o -E '[0-9\.]+')

get IP

$ curl IP

$ curl IP/index.php

$ curl IP/connect.php