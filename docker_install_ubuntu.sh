#!/bin/bash

#### Check if root ####
if ! [ $(id -u) = 0 ]; then
   echo "You need to be a root!"
   exit 1
fi
#### Update local database ####
apt-get update
#### Download dependencies ####
apt-get install apt-transport-https ca-certificates curl software-properties-common
#### Add docker’s GPG key ####
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add
#### Install the docker repository ####
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
#### Update repositories ####
apt-get update
#### Install latest version of docker ####
apt-get install docker-ce
#### Start and automate docker ####
systemctl start docker
systemctl enable docker
#### Check docker version ####
docker --version
#### End ####
echo "All done !"
exit 0